import org.testng.annotations.Test;
import static org.testng.Assert.assertTrue;

public class SearchImageTests extends BaseTest {
    private final String GOOGLE_URL = "https://www.google.com/";

    private final String WORD="image";

    @Test(priority = 1)
    public void checkSearchImage() {
        getGooglePage().openGooglePage(GOOGLE_URL);
        getGooglePage().isSearchFieldVisible();
        getGooglePage().inputSearchFieldVisible(WORD);
        assertTrue(getGooglePage().getSearchResultsList().size()>10);
    }
}
