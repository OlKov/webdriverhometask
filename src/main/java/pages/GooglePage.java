package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.openqa.selenium.By.xpath;

public class GooglePage extends BasePage {

    private static final String SEARCH_FIELD = "//input[@class='gLFyf gsfi']";

    private static final String IMAGE="//img[contains(@src,'data:image/jpeg')]";

    public void isSearchFieldVisible() {
        driver.findElement(xpath(SEARCH_FIELD)).isDisplayed();
    }

    public void inputSearchFieldVisible(final String keyword) {

        driver.findElement(xpath(SEARCH_FIELD)).sendKeys(keyword, Keys.ENTER);
    }

    public GooglePage(WebDriver driver) {
        super(driver);
    }

    public void openGooglePage(String url) {
        driver.get(url);
    }

    public List<WebElement> getSearchResultsList() {
        return driver.findElements(xpath(IMAGE));
    }
}
